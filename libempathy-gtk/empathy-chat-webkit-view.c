/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2002-2007 Imendio AB
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 * 
 * Authors: Mikael Hallendal <micke@imendio.com>
 *          Richard Hult <richard@imendio.com>
 *          Martyn Russell <martyn@imendio.com>
 */

#include "config.h"

#include <sys/types.h>
#include <string.h>
#include <time.h>
#include <gtk/gtk.h>
#include <glib/gi18n.h>
#include <gtk/gtkbutton.h>
#include <gtk/gtkimage.h>
#include <gtk/gtkmenu.h>
#include <gtk/gtkmenuitem.h>
#include <gtk/gtkimagemenuitem.h>
#include <gtk/gtkstock.h>
#include <gtk/gtkscrolledwindow.h>
#include <gtk/gtksizegroup.h>
#include <glade/glade.h>

#include <telepathy-glib/util.h>
#include <libmissioncontrol/mc-account.h>

#include <libempathy/empathy-utils.h>
#include <libempathy/empathy-debug.h>

#include "empathy-chat.h"
#include "empathy-conf.h"

#include "empathy-ui-utils.h"
#include "empathy-smiley-manager.h"

#include "empathy-chat-webkit-view.h"
//#include "empathy-smiley-manager.h"

//#define DEBUG_DOMAIN "ChatViewWebKit"
#define DEBUG_FLAG EMPATHY_DEBUG_CHAT
#include <libempathy/empathy-debug.h>



#define GET_PRIV(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj),		\
						    EMPATHY_TYPE_CHAT_WEBKIT_VIEW, EmpathyChatWebkitViewPriv))

struct _EmpathyChatWebkitViewPriv {
        gchar          *lists;
        EmpathyContact *last_contact;

        gchar          *status_html;
        gchar *incoming_content_html;
        gchar *incoming_next_content_html;
        gchar *outgoing_content_html;
        gchar *outgoing_next_content_html;

//	GtkTextBuffer *buffer;
        EmpathySmileyManager *smiley_manager;
        gboolean first;
//	WebKitPage *view;
	WebKitWebView *view;
	
};

static void empathy_chat_webkit_view_class_init (EmpathyChatWebkitViewClass *klass);
static void empathy_chat_webkit_view_init       (EmpathyChatWebkitView      *manager);
static void theme_webkit_finalize               (GObject *object);

//static void theme_webkit_size_allocate        (GtkWidget *widget, GtkAllocation *alloc);


static void empathy_chat_webkit_view_append_message (EmpathyChatView *view,
						     EmpathyMessage  *msg);
static void empathy_chat_webkit_view_append_event   (EmpathyChatView *view,
						     const gchar     *str);
//void             empathy_chat_webkit_view_set_margin           (EmpathyChatViewa *view,
//							 gint             margin);
//void             empathy_chat_webkit_view_scroll               (EmpathyChatView *view,
//							 gboolean         allow_scrolling);
//void             empathy_chat_webkit_view_scroll_down          (EmpathyChatView *view);
//gboolean         empathy_chat_webkit_view_get_selection_bounds (EmpathyChatView *view,
//							 GtkTextIter     *start,
//							 GtkTextIter     *end);
//void             empathy_chat_webkit_view_clear                (EmpathyChatView *view);
//void             empathy_chat_webkit_view_copy_clipboard       (EmpathyChatView *view);

void chat_webkit_view_set_theme (EmpathyChatWebkitView *view,
				 const gchar *theme,
				 const gchar *ccs_style);


static WebKitNavigationResponse
webkit_navigation_requested_cb (WebKitWebView* web_view, 
				WebKitWebFrame* frame, 
				WebKitNetworkRequest* request);
static gchar *replace_header_tokens (gchar *text, gsize len, gpointer *conv);
static gchar *replace_template_tokens (gchar *text, gint len, gchar *header, gchar *footer, gchar *template_path, gchar *basestyle_css);
static gchar *replace_message_tokens (gchar *text, gsize len, gpointer *conv, const gchar *name, const gchar *alias,
                                      const gchar *message, gpointer flags, time_t timestamp);
static char *escape_message (gchar *text, gpointer *gx);


static void theme_webkit_iface_init (EmpathyChatViewIface    *iface);
G_DEFINE_TYPE_WITH_CODE (EmpathyChatWebkitView, empathy_chat_webkit_view, /*GTK_TYPE_HBOX*/ /*GTK_TYPE_SCROLLED_WINDOW*/ /*WEBKIT_TYPE_PAGE*/ WEBKIT_TYPE_WEB_VIEW ,
                         G_IMPLEMENT_INTERFACE (EMPATHY_TYPE_CHAT_VIEW,
                                                theme_webkit_iface_init) );

static void
empathy_chat_webkit_view_class_init (EmpathyChatWebkitViewClass *klass)
{
        GObjectClass *object_class = G_OBJECT_CLASS (klass);
//        GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
	
        object_class->finalize = theme_webkit_finalize;
//        widget_class->size_allocate = theme_webkit_size_allocate;

        g_type_class_add_private (object_class, sizeof (EmpathyChatWebkitViewPriv) );
}

void 
chat_webkit_view_set_theme (EmpathyChatWebkitView *view,
			    const gchar *style,
			    const gchar *ccs_style)
{
	EmpathyChatWebkitViewPriv *priv;
	priv = GET_PRIV (view);

	gchar *style_dir = "/mnt/disk/src/gnome/webkitstyle";

        gchar *file = NULL;
        gchar *template_path = NULL;
        gchar *basestyle_css = NULL;
        gchar *template_html = NULL, *header_html = NULL, *footer_html = NULL;
        gsize template_html_len = 0, header_html_len = 0, footer_html_len = 0;
        gsize /*basestyle_css_len = 0,*/ status_html_len = 0, incoming_content_html_len = 0, incoming_next_content_html_len = 0,
		outgoing_content_html_len = 0, outgoing_next_content_html_len = 0;
	//GError *error = NULL; 
        template_path = g_build_filename (style_dir, style ,"Contents", "Resources", "Template.html", NULL);
	if (!g_file_test (template_path, G_FILE_TEST_IS_REGULAR))
		template_path = g_build_filename (style_dir, style ,"Contents", "Resources", "_template.html", NULL);
	if (!g_file_test (template_path, G_FILE_TEST_IS_REGULAR))
		template_path = g_build_filename (style_dir, "default" ,"Contents", "Resources", "Template.html", NULL);
		
        if (!g_file_get_contents (template_path, &template_html, &template_html_len, NULL));//
	{
		DEBUG ("ERROR %s\n ", template_path);
		//g_error_free (error);
	}
	//g_assert ((template_html == NULL && error != NULL) || (template_html != NULL && error == NULL));

        file = g_build_filename (style_dir, style, "Contents", "Resources", "Header.html", NULL);
        g_file_get_contents (file, &header_html, &header_html_len, NULL);
	g_free (file);

        file = g_build_filename (style_dir, style, "Contents", "Resources", "Footer.html", NULL);
        g_file_get_contents (file, &footer_html, &footer_html_len, NULL);
	g_free (file);

        file = g_build_filename (style_dir, style, "Contents", "Resources", "Outgoing", "Content.html", NULL);
        g_file_get_contents (file, &priv->outgoing_content_html, &outgoing_content_html_len, NULL);//
	g_free (file);

        file = g_build_filename (style_dir, style, "Contents", "Resources", "Outgoing", "NextContent.html", NULL);
        if (!g_file_get_contents (file, &priv->outgoing_next_content_html, &outgoing_next_content_html_len, NULL) ) {
//                priv->outgoing_next_content_html = priv->outgoing_content_html;
//                outgoing_next_content_html_len = outgoing_content_html_len;
        }
	g_free (file);

        file = g_build_filename (style_dir, style, "Contents", "Resources", "Incoming", "Content.html", NULL);
        g_file_get_contents (file, &priv->incoming_content_html, &incoming_content_html_len, NULL);//
	g_free (file);

        file = g_build_filename (style_dir, style, "Contents", "Resources", "Incoming", "NextContent.html", NULL);
        if (!g_file_get_contents (file, &priv->incoming_next_content_html, &incoming_next_content_html_len, NULL) ) {
//                priv->incoming_next_content_html = priv->incoming_content_html;
//                incoming_next_content_html_len = incoming_content_html_len;
        }
	g_free (file);

        file = g_build_filename (style_dir, style, "Contents", "Resources", "Status.html", NULL);
        g_file_get_contents (file, &priv->status_html, &status_html_len, NULL); //
	g_free (file);

        //~ file = g_build_filename (style_dir, style, "Contents", "Resources", "main.css", NULL);
        //~ g_file_get_contents (file, &basestyle_css, &basestyle_css_len, NULL);//
	basestyle_css = g_build_filename ("main.css", NULL);
	//basestyle_css = g_build_filename ("style", "smoke.css", NULL);

        char *header;
        char *footer;
        char *template;
	
	template_path = g_build_filename (style_dir, style ,"Contents", "Resources", "Status.html", NULL);

        header = replace_header_tokens (header_html, header_html_len, NULL);
        footer = replace_header_tokens (footer_html, footer_html_len, NULL);
        template = replace_template_tokens (template_html, 
					    template_html_len + header_html_len + footer_html_len, 
					    header, footer, 
					    template_path,
					    basestyle_css);

	webkit_web_view_load_string(WEBKIT_WEB_VIEW(priv->view), template, "text/html", "UTF-8", template_path);

	g_free(template_path);
	g_free(template_html);
	g_free(header_html);
	g_free(footer_html);
	g_free(basestyle_css);

	g_free(header);
	g_free(footer);
	g_free(template);

}

static void
empathy_chat_webkit_view_init (EmpathyChatWebkitView *view)
{
	
//	DEBUG ("empathy_chat_webkit_view_init initttttttt");
        EmpathyChatWebkitViewPriv *priv;
        priv = GET_PRIV (view);
        priv->smiley_manager = empathy_smiley_manager_new ();

        priv->last_contact = NULL;

	priv->view = WEBKIT_WEB_VIEW (view);

	chat_webkit_view_set_theme (view , "default", NULL);

//	gchar *msg;
//        gchar *escape;
//        gchar *script;
//        gchar *func = "appendMessage";
//        msg = replace_message_tokens (priv->status_html, 0, NULL, "name", "Info", "Funca", NULL, 0);
//        escape = escape_message (msg, NULL);
//        script = g_strdup_printf ("%s(\"%s\")", func, escape);
////        webkit_web_view_execute_script (WEBKIT_WEB_VIEW (view), script);
//	  
//	DEBUG ("\n\nempathy_chat_webkit_view_append_message %s\n %s\n %s\n\n", func, "name", script);

        g_signal_connect (G_OBJECT (view), "navigation-requested", G_CALLBACK (webkit_navigation_requested_cb), NULL);
        priv->first = TRUE;
//	DEBUG ("empathy_chat_webkit_view_init FIN INIT  \n ");
}

static void
theme_webkit_finalize (GObject *object)
{
        EmpathyChatWebkitView     *view;
        EmpathyChatWebkitViewPriv *priv;

        view = EMPATHY_CHAT_WEBKIT_VIEW (object);
        priv = GET_PRIV (view);

        if (priv->last_contact) {
                g_object_unref (priv->last_contact);
                priv->last_contact = NULL;
        }

        if (priv->smiley_manager) {
                g_object_unref (priv->smiley_manager);
        }

	if (priv->status_html)
		g_free (priv->status_html);
	if (priv->incoming_content_html)
		g_free (priv->incoming_content_html);
	if (priv->incoming_next_content_html)
		g_free (priv->incoming_next_content_html);
	if (priv->outgoing_content_html)
		g_free (priv->outgoing_content_html);
	if (priv->outgoing_next_content_html)
		g_free (priv->outgoing_next_content_html);

        G_OBJECT_CLASS (empathy_chat_webkit_view_parent_class)->finalize (object);
}

//static void
//theme_webkit_size_allocate (GtkWidget     *widget,
//                            GtkAllocation *alloc)
//{
//        //GTK_WIDGET_CLASS (empathy_chat_webkit_view_parent_class)->size_allocate (widget, alloc);
//}

EmpathyChatWebkitView *
empathy_chat_webkit_view_new (void)
{
        return g_object_new (EMPATHY_TYPE_CHAT_WEBKIT_VIEW, NULL);
}

static WebKitNavigationResponse
webkit_navigation_requested_cb (WebKitWebView* web_view, WebKitWebFrame* frame, WebKitNetworkRequest* request)
{
//         TODO: Adium does not open file: URIs and allows WebKit to handle any navigation of type 'Other' 
//	purple_notify_uri(NULL, webkit_network_request_get_uri(request));
        return WEBKIT_NAVIGATION_RESPONSE_IGNORE;
}


static char *
replace_header_tokens (gchar *text, gsize len, gpointer *conv)
{
        GString *str = g_string_new_len (NULL, len);
        gchar *cur = text;
        gchar *prev = cur;

        if (text == NULL)
                return NULL;

        while ( (cur = strchr (cur, '%') ) ) {
                const gchar *replace = NULL;
                gchar *fin = NULL;

                if (!strncmp (cur, "%chatName%", strlen ("%chatName%") ) ) {
//			replace = conv->name;
                } else if (!strncmp (cur, "%sourceName%", strlen ("%sourceName%") ) ) {
//			replace = purple_account_get_alias(conv->account);
//			if (replace == NULL)
//				replace = purple_account_get_username(conv->account);
                } else if (!strncmp (cur, "%destinationName%", strlen ("%destinationName%") ) ) {
//			PurpleBuddy *buddy = purple_find_buddy(conv->account, conv->name);
//			if (buddy) {
//				replace = purple_buddy_get_alias(buddy);
//			} else {
//				replace = conv->name;
//			}
                } else if (!strncmp (cur, "%incomingIconPath%", strlen ("%incomingIconPath%") ) ) {
//			PurpleBuddyIcon *icon = purple_conv_im_get_icon(PURPLE_CONV_IM(conv));
//			replace = purple_buddy_icon_get_full_path(icon);
			replace = "/mnt/disk/src/gnome/webkitstyle/default/Contents/Resources/incoming_icon.png";
                } else if (!strncmp (cur, "%outgoingIconPath%", strlen ("%outgoingIconPath%") ) ) {
			
                } else if (!strncmp (cur, "%timeOpened", strlen ("%timeOpened") ) ) {
                        gchar *format = NULL;
                        if (* (cur + strlen ("%timeOpened") ) == '{') {
                                gchar *start = cur + strlen ("%timeOpened") + 1;
                                gchar *end = strstr (start, "}%");
                                if (!end) /* Invalid string */
                                        continue;
                                format = g_strndup (start, end - start);
                                fin = end + 1;
                        }
//			replace = purple_utf8_strftime(format ? format : "%X", NULL);
                        g_free (format);
                } else {
			cur++;
                        continue;
                }

                /* Here we have a replacement to make */
                g_string_append_len (str, prev, cur - prev);
                g_string_append (str, replace);

                /* And update the pointers */
                if (fin) {
                        prev = cur = fin + 1;
                } else {
                        prev = cur = strchr (cur + 1, '%') + 1;
                }
        }

        /* And wrap it up */
        g_string_append (str, prev);
        return g_string_free (str, FALSE);
}

static char *
replace_template_tokens (gchar *text, gint len, gchar *header, gchar *footer, gchar *template_path, gchar *css_path)
{
        GString *str = g_string_new_len (NULL, len);

        gchar **ms = g_strsplit (text, "%@", 5);

        if (ms[0] == NULL || ms[1] == NULL || ms[2] == NULL || ms[3] == NULL || ms[4] == NULL /*|| ms[5] == NULL*/) {
                g_strfreev (ms);
                g_string_free (str, TRUE);
                return NULL;
        }
	int i=0;

        g_string_append (str, ms[i++]);
        g_string_append (str, template_path);
        //~ g_string_append (str, ms[i++]);
        //~ if (basestyle_css)
                //~ g_string_append (str, basestyle_css);
        g_string_append (str, ms[i++]);
	if (css_path)
		g_string_append(str, css_path);
        g_string_append (str, ms[i++]);
        if (header)
                g_string_append (str, header);
        g_string_append (str, ms[i++]);
        if (footer)
                g_string_append (str, footer);
        g_string_append (str, ms[i++]);

        g_strfreev (ms);
        return g_string_free (str, FALSE);
}

static char *
replace_message_tokens (gchar *text, gsize len, gpointer *conv, const gchar *name, const char *alias,
                        const gchar *message, gpointer flags, time_t timestamp)
{
        GString *str = g_string_new_len (NULL, len);
        gchar *cur = text;
        gchar *prev = cur;

        while ( (cur = strchr (cur, '%') ) ) {
                const gchar *replace = NULL;
                gchar *fin = NULL;

                if (!strncmp (cur, "%message%", strlen ("%message%") ) ) {
                        replace = message;
                } else if (!strncmp (cur, "%messageClasses%", strlen ("%messageClasses%") ) ) {
                        replace = /*flags & PURPLE_MESSAGE_SEND ? */"outgoing" /*:
										 flags & PURPLE_MESSAGE_RECV ? "incoming" : "event"*/;
                } else if (!strncmp (cur, "%time", strlen ("%time") ) ) {
                        replace = empathy_time_to_string_local (timestamp, EMPATHY_TIME_FORMAT_DISPLAY_SHORT);
		} else if (!strncmp(cur, "%userIconPath%", strlen("%userIconPath%"))) {
			replace = "/mnt/disk/src/gnome/webkitstyle/default/Contents/Resources/incoming_icon.png";
//			if (flags & PURPLE_MESSAGE_SEND) {
//				if (purple_account_get_bool(conv->account, "use-global-buddyicon", TRUE)) {
//					replace = purple_prefs_get_path(PIDGIN_PREFS_ROOT "/accounts/buddyicon");
//				} else {
//					PurpleStoredImage *img = purple_buddy_icons_find_account_icon(conv->account);
//					replace = purple_imgstore_get_filename(img);
//				}
//				if (replace == NULL || !g_file_test(replace, G_FILE_TEST_EXISTS)) {
//					replace = g_build_filename(style_dir, "Contents", "Resources",
//								   "Outgoing", "buddy_icon.png", NULL);
//				}
//			} else if (flags & PURPLE_MESSAGE_RECV) {
//				PurpleBuddyIcon *icon = purple_conv_im_get_icon(PURPLE_CONV_IM(conv));
//				replace = purple_buddy_icon_get_full_path(icon);
//				if (replace == NULL || !g_file_test(replace, G_FILE_TEST_EXISTS)) {
//					replace = g_build_filename(style_dir, "Contents", "Resources",
//								   "Incoming", "buddy_icon.png", NULL);
//				}
//			}
		} else if (!strncmp(cur, "%senderScreenName%", strlen("%senderScreenName%"))) {
//			replace = name;
                } else if (!strncmp (cur, "%sender%", strlen ("%sender%") ) ) {
                        replace = alias;
		} else if (!strncmp(cur, "%service%", strlen("%service%"))) {
//			replace = purple_account_get_protocol_name(conv->account);
			replace = "XMMP";
                } else {
                        cur++;
                        continue;
                }

                /* Here we have a replacement to make */
                g_string_append_len (str, prev, cur - prev);
                g_string_append (str, replace);

                /* And update the pointers */
                if (fin) {
                        prev = cur = fin + 1;
                } else {
                        prev = cur = strchr (cur + 1, '%') + 1;
                }

        }

        /* And wrap it up */
        g_string_append (str, prev);
        return g_string_free (str, FALSE);
}

static gchar *escape_message (gchar *text, gpointer *gx)
{
        GString *str = g_string_new (NULL);
        gchar *cur = text;
//	int smileylen = 0;

        while (cur && *cur) {
                switch (*cur) {
                case '\\':
                        g_string_append (str, "\\\\");
                        break;
                case '\"':
                        g_string_append (str, "\\\"");
                        break;
                case '\r':
                        g_string_append (str, "<br/>");
                        break;
                case '\n':
                        break;
                default:
//			if (gx && gtk_imhtml_is_smiley(GTK_IMHTML(gx->imhtml), NULL, cur, &smileylen)) {
//				char *smiley_markup, *unescaped;
//				char *ws = g_malloc(smileylen * 5);
//				g_snprintf (ws, smileylen + 1, "%s", cur);
//			        unescaped = purple_unescape_html(ws);

//				GtkIMHtmlSmiley *imhtml_smiley = gtk_imhtml_smiley_get(GTK_IMHTML(gx->imhtml), NULL, unescaped);
//				smiley_markup = g_strdup_printf("<abbr title='%s'><img src='file://%s' alt='%s'/></abbr>",
//								 imhtml_smiley->smile, imhtml_smiley->file, imhtml_smiley->smile);
//				g_string_append(str, smiley_markup);
//				cur += smileylen - 1;
//			} else {
                        g_string_append_c (str, *cur);
//			}
                }
                cur++;
        }
        return g_string_free (str, FALSE);
}

EmpathyContact *
empathy_chat_webkit_view_get_last_contact (EmpathyChatWebkitView *view)
{
        EmpathyChatWebkitViewPriv *priv;

        g_return_val_if_fail (EMPATHY_IS_CHAT_WEBKIT_VIEW (view), NULL);

        priv = GET_PRIV (view);

        return priv->last_contact;
}

void
empathy_chat_webkit_view_append_event (EmpathyChatView *view,
                                       const gchar     *str)
{
        EmpathyChatWebkitViewPriv *priv;
//	gboolean            bottom;

        gchar *stripped;
        gchar *message_html;
        gchar *msg;
        gchar *escape;
        gchar *script;
        gchar *func = "appendMessage";

        g_return_if_fail (EMPATHY_IS_CHAT_VIEW (view) );
        g_return_if_fail (!G_STR_EMPTY (str) );

        priv = GET_PRIV (view);

        message_html = priv->status_html;
        stripped = g_strdup (str);

        msg = replace_message_tokens (message_html, 0, NULL, "name", "Info", stripped, NULL, 0);
        escape = escape_message (msg, NULL);
        script = g_strdup_printf ("%s(\"%s\")", func, escape);
        //webkit_page_execute_script (WEBKIT_PAGE (view), script);
        //DEBUG ("\n\nempathy_chat_webkit_view_append_event %s\n %s\n %s\n\n", func, "name", script);


        g_free (msg);
        g_free (stripped);
        g_free (escape);
        g_free (script);


//	bottom = chat_view_is_scrolled_down (EMPATHY_CHAT_VIEWA(view));

//	chat_view_maybe_trim_buffer (view);

//	empathy_theme_append_event (priv->theme, EMPATHY_CHAT_VIEWA(view), str);

//	if (bottom) {
//		empathy_chat_viewa_scroll_down (EMPATHY_CHAT_VIEW(view));
//	}

        if (priv->last_contact) {
                g_object_unref (priv->last_contact);
                priv->last_contact = NULL;
        }
}

static void
empathy_chat_webkit_view_append_message (EmpathyChatView *view,
					 EmpathyMessage  *emsg)
{
        EmpathyChatWebkitViewPriv *priv = GET_PRIV (view);

//	gboolean             bottom;
        //PurpleConversationType type;
//	GtkWidget *webkit;
        gchar *stripped;
        gchar *message_html = NULL;
        gchar *msg;
        gchar *escape;
        gchar *script;
        gchar *func = "appendMessage";
        time_t timestamp;

        EmpathyContact        *contact;
        const gchar          *name;
	EmpathyContact        *last_contact;
        gboolean              new_sender = TRUE;

	GString *buffer = NULL;

        g_return_if_fail (EMPATHY_IS_CHAT_WEBKIT_VIEW (view) );
        g_return_if_fail (EMPATHY_IS_MESSAGE (emsg) );

        if (!empathy_message_get_body (emsg) ) {
                return;
        }

	gchar **spl;
	gint len;
	gint i;
	spl = g_strsplit (empathy_message_get_body (emsg), "\n", -1);
	len = g_strv_length (spl);
	i = 0;
	buffer = g_string_new ("");
	g_string_append (buffer, spl[i++]);
//	stripped = g_strdup (spl[i++]);
	if (len > 1)
		while (spl[i]){
			g_string_append_printf (buffer, "<br/>%s", spl[i++]);
		}
	stripped = g_string_free (buffer, FALSE);
//        stripped = g_strdup (empathy_message_get_body (emsg) ); //purple_markup_strip_html(message);

        empathy_message_get_date_and_time (emsg, &timestamp);
        contact = empathy_message_get_sender (emsg);
        name = empathy_contact_get_name (contact);

	last_contact = empathy_chat_webkit_view_get_last_contact (EMPATHY_CHAT_WEBKIT_VIEW (view));

        if (priv->first == FALSE && last_contact && contact && empathy_contact_equal (last_contact, contact) ){
                new_sender = FALSE;
	}
	priv->first = FALSE;

	buffer = g_string_new ("");
        GSList *smileys, *l;
        smileys = empathy_smiley_manager_parse (priv->smiley_manager, stripped);
        for (l = smileys; l; l = l->next) {
                EmpathySmiley *smiley;

                smiley = l->data;
                if (smiley->pixbuf) {
//			DEBUG ("empathy_chat_webkit_view_append_message si\t %s", smiley->str);
//                      g_string_append_printf (buffer, "smile %s %s", smiley->str, smiley->path);
			g_string_append_printf (buffer, "<img src=\"%s\"/>", smiley->path);
//			DEBUG ("%s\n", smiley->path);
                } else {
//			gtk_text_buffer_insert (buf, iter, smiley->str, -1);
//			DEBUG ("empathy_chat_webkit_view_append_message no\t %s", smiley->str);
                        g_string_append_printf (buffer, "%s", smiley->str);
                }
                empathy_smiley_free (smiley);
        }
        g_slist_free (smileys);

        g_free (stripped);
        stripped = g_string_free (buffer, FALSE);

        if (message_html == NULL) {
                if (!new_sender) {
                        message_html = priv->outgoing_next_content_html;
                        func = "appendNextMessage";
                } else { /*if (flags & PURPLE_MESSAGE_SEND) */
                        message_html = priv->outgoing_content_html;
//	} else if (flags & PURPLE_MESSAGE_RECV && old_flags & PURPLE_MESSAGE_RECV) {
//		message_html = priv->incoming_next_content_html;
//		func = "appendNextMessage";
//	} else if (flags & PURPLE_MESSAGE_RECV) {
//		message_html = priv->incoming_content_html;
//	} else {
//		message_html = priv->status_html;
                }
        }


        msg = replace_message_tokens (message_html, 0, NULL, "name", name, stripped, NULL, timestamp);
        escape = escape_message (msg, /*g_hash_table_lookup(webkits, conv)*/ NULL);
        script = g_strdup_printf ("%s(\"%s\")", func, escape);
        webkit_web_view_execute_script (WEBKIT_WEB_VIEW (priv->view), script);
	DEBUG ("\n\nempathy_chat_webkit_view_append_message %s\n %s\n %s\n\n", func, name, script);
	
        g_free (msg);
        g_free (stripped);
        g_free (escape);
        g_free (script);

        if (priv->last_contact) {
                g_object_unref (priv->last_contact);
        }
        priv->last_contact = g_object_ref (empathy_message_get_sender (emsg) );
	//webkit_page_reload (WEBKIT_PAGE (view));
}

static void
theme_webkit_iface_init (EmpathyChatViewIface *iface)
{
        iface->append_event = empathy_chat_webkit_view_append_event;
//	iface->clear = empathy_chat_viewa_clear;
        iface->append_message = empathy_chat_webkit_view_append_message;
//	iface->get_smiley_menu = empathy_chat_webkit_view_get_smiley_menu;
//	iface->scroll = empathy_chat_viewa_scroll;
//	iface->scroll_down = empathy_chat_viewa_scroll_down;
//	iface->get_selection_bounds = empathy_chat_viewa_get_selection_bounds;
//	iface->copy_clipboard = empathy_chat_viewa_copy_clipboard;
}

