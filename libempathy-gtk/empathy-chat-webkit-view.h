/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2007 Collabora Ltd.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * Authors: Juan Pizarro <jpizarro@gmail.com>
 */

#ifndef __EMPATHY_CHAT_WEBKIT_VIEW_H__
#define __EMPATHY_CHAT_WEBKIT_VIEW_H__

#include <glib.h>
#include <gtk/gtk.h>

/* Webkit headers */
//#include <webkitgtkpage.h>
//#include <webkit/webkit.h>
#include <webkit/webkitwebview.h>
//#include <webkit/webkitnetworkrequest.h>

G_BEGIN_DECLS

#define EMPATHY_TYPE_CHAT_WEBKIT_VIEW         (empathy_chat_webkit_view_get_type ())
#define EMPATHY_CHAT_WEBKIT_VIEW(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), EMPATHY_TYPE_CHAT_WEBKIT_VIEW, EmpathyChatWebkitView))
#define EMPATHY_CHAT_WEBKIT_VIEW_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), EMPATHY_TYPE_CHAT_WEBKIT_VIEW, EmpathyChatWebkitViewClass))
#define EMPATHY_IS_CHAT_WEBKIT_VIEW(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), EMPATHY_TYPE_CHAT_WEBKIT_VIEW))
#define EMPATHY_IS_CHAT_WEBKIT_VIEW_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), EMPATHY_TYPE_CHAT_WEBKIT_VIEW))
#define EMPATHY_CHAT_WEBKIT_VIEW_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), EMPATHY_TYPE_CHAT_WEBKIT_VIEW, EmpathyChatWebkitViewClass))

typedef struct _EmpathyChatWebkitView      EmpathyChatWebkitView;
typedef struct _EmpathyChatWebkitViewClass EmpathyChatWebkitViewClass;
typedef struct _EmpathyChatWebkitViewPriv  EmpathyChatWebkitViewPriv;

struct _EmpathyChatWebkitView {
//    WebKitPage		parent;
    //~ GtkScrolledWindow	parent;
    //GtkHBox 			parent;
	WebKitWebView parent;
};

struct _EmpathyChatWebkitViewClass {
//    WebKitPageClass	parent_class;
    //~ GtkScrolledWindowClass	parent_class;
    //GtkHBoxClass	parent_class;
	WebKitWebViewClass parent_class;
};

GType                empathy_chat_webkit_view_get_type (void) G_GNUC_CONST;
EmpathyChatWebkitView * empathy_chat_webkit_view_new      (void);

EmpathyContact * empathy_chat_webkit_view_get_last_contact (EmpathyChatWebkitView *view);
GtkWidget *      empathy_chat_webkit_view_get_smiley_menu      (GCallback        callback,
        gpointer         user_data);

G_END_DECLS

#endif /* __EMPATHY_CHAT_WEBKIT_VIEW_H__ */
