#include <gtk/gtk.h>
#include <glib/gprintf.h>

#include <libempathy-gtk/empathy-chat-view.h>

static void hello( GtkWidget *widget,
                   gpointer   data )
{
	EmpathyChatView *view;
	view = EMPATHY_CHAT_VIEW (data);
	EmpathyMessage  *message;
	message = empathy_message_new ("jeje");
	empathy_chat_view_append_message (view , message);
	g_print ("Hello World\n");
}

static gboolean delete_event( GtkWidget *widget,
                              GdkEvent  *event,
                              gpointer   data )
{
    /* If you return FALSE in the "delete_event" signal handler,
     * GTK will emit the "destroy" signal. Returning TRUE means
     * you don't want the window to be destroyed.
     * This is useful for popping up 'are you sure you want to quit?'
     * type dialogs. */

    g_print ("delete event occurred\n");

    /* Change TRUE to FALSE and the main window will be destroyed with
     * a "delete_event". */

    return FALSE;
}

/* Another callback */
static void destroy( GtkWidget *widget,
                     gpointer   data )
{
    gtk_main_quit ();
}

int main(int argc, char **argv){
	gtk_init(&argc, &argv);
	EmpathyChatView *view;
	GtkWidget *window, *button, *hbox; 

	view = empathy_chat_view_new ();
	g_printf("gg\n");;
	
//	view = empathy_chat_view_new ();

    /* create a new window */
	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_default_size (GTK_WINDOW (window) , 800, 600);
    
    /* When the window is given the "delete_event" signal (this is given
     * by the window manager, usually by the "close" option, or on the
     * titlebar), we ask it to call the delete_event () function
     * as defined above. The data passed to the callback
     * function is NULL and is ignored in the callback function. */
    g_signal_connect (G_OBJECT (window), "delete_event",
		      G_CALLBACK (delete_event), NULL);
    
    /* Here we connect the "destroy" event to a signal handler.  
     * This event occurs when we call gtk_widget_destroy() on the window,
     * or if we return FALSE in the "delete_event" callback. */
    g_signal_connect (G_OBJECT (window), "destroy",
		      G_CALLBACK (destroy), NULL);
    
    /* Sets the border width of the window. */
    gtk_container_set_border_width (GTK_CONTAINER (window), 10);
    
    /* Creates a new button with the label "Hello World". */
    button = gtk_button_new_with_label ("Hello World");
    
    /* When the button receives the "clicked" signal, it will call the
     * function hello() passing it NULL as its argument.  The hello()
     * function is defined above. */
    g_signal_connect (G_OBJECT (button), "clicked",
		      G_CALLBACK (hello), view);
    
    /* This will cause the window to be destroyed by calling
     * gtk_widget_destroy(window) when "clicked".  Again, the destroy
     * signal could come from here, or the window manager. */
/*    g_signal_connect_swapped (G_OBJECT (button), "clicked",
			      G_CALLBACK (gtk_widget_destroy),
                              G_OBJECT (window));*/
    
    /* This packs the button into the window (a gtk container). */
    hbox = gtk_hbox_new (TRUE, 10);
    gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);

    gtk_container_add (GTK_CONTAINER (window), GTK_WIDGET (hbox));

    gtk_box_pack_start(GTK_BOX(hbox), GTK_WIDGET (view), TRUE, TRUE, 0);
    gtk_widget_show_all (GTK_WIDGET (view));

    /* The final step is to display this newly created widget. */
    gtk_widget_show (button);
    
    /* and the window */
    gtk_widget_show_all (window);
    
    EmpathyMessage  *message;
    message = empathy_message_new ("jojo");
    empathy_chat_view_append_message (view , message);

   /* All GTK applications must have a gtk_main(). Control ends here
     * and waits for an event to occur (like a key press or
     * mouse event). */
    gtk_main ();
    
    return 0;
}
